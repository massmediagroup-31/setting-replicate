>docker-compose build

>docker-compose up -d

Get ip4 from master
>docker inspect replicate_overlay


Run console session into master container
>docker exec -it mysql_master mysql -uroot -ppassword

Get log pos and log file from command into master mysql console session:
>show master status\G

Setup slave 
>docker exec -it mysql_slave mysql -uroot -ppassword

Settings replicate into slave console session
>CHANGE MASTER TO MASTER_HOST='master-ip4', MASTER_USER='root', MASTER_PASSWORD='password', MASTER_LOG_FILE='log-file', MASTER_LOG_POS=pos;

>start slave;

>show slave status\G

Create table
>docker exec mysql_master sh -c "mysql -u root -ppassword replicate_db -e 'create table articles(code varchar(30))'"

Insert data into master mysql container
>docker exec mysql_master sh -c "mysql -u root -ppassword replicate_db -e 'insert articles values (111)'"

Check data in slave
>docker exec mysql_slave sh -c "mysql -u root -ppassword replicate_db -e 'select * from articles\G'"
